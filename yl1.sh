#!/bin/bash
#Muuda "file" muutuja enda faili asukoha j2rgi!
file=/home/student/skriptimine/test.txt
#file=/etc/bind/zones/db.autun.hom
#Kontroll, et sisendeid oleks 2

if [ $# -eq 2 ]
	then
	ADDR=$1
	HOSTN=$2
else
	echo "Sisesta palun sisendid korrektselt (IPADDRESS HOSTINIMI)."
	exit 1
fi

#V2ike grep otsing, et leida etteantud failist samu kirjeid ja see rida v2lja printida.
crepe="$(grep -C0 -e $HOSTN -e $ADDR $file)"

# IP ADDRESS kontoll

ipvalid() {
	local ip=${1:-1.2.3.4}
	IFS=.; local -a a=($ip)
	[[ $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]] || return 1
	for quad in {0..3}; do
		[[ "${a[$quad]}" -gt 255 ]] && return 1
	done
	return 0
}

#Uute sisendite kysimine, IP kontroll, failis olemasolevate kirjete kontroll, kirjete lisamine faili

cases(){
		echo "${crepe}"
		printf "Kirje on juba olemas, kas soovite muuta v6i lisada? \nY (muuda)\nN (v2lju)\n"
		read soov
		case $soov in
			[yY] )
				#Jah puhul saab kasutaja valida, kumba osa kirjest muudab
				printf "Muuda \n[1] IP\n[2] HOSTNAME"
				read soow
				if [[ "$soow" -eq 1 ]] ; then
					echo "Sisestage uus IP"
					read uusip
			  		until ipvalid "$uusip" ; do
						echo "Sisestage (uus) IP aadress:"
						read uusip
					done
					uus="$HOSTN			$uusip"
					
				elif [[ "$soow" -eq 2 ]]; then
					echo "Sisestage (uus) hostname:"
					read uushost
					
					uus="$uushost			$ADDR"
				fi
			perl -pi -e "s/${crepe}/$uus/" "$file"
			
		;;
#		[lL] )
			#lisa puhul saab kasutaja lisada t2iesti uue kirje. NB! See jupike koodi ei tee seda mida ma tahaksin, nimelt "grep" k2sk talle just siin jupis miskip2rast ei sobi. Kui Kellelgi j22b miskit silma, siis andke tagasisides teada. Ait2h!
#			echo "Sisestage uus IP"
#			read uusip
#			until ipvalid "$uusip" ; do 
#				echo "Sisestage uus IP"
#				read uusip
#			done
			
#			echo "Sisestage uus HOSTNAME"
#			read uushost
#			uus="$uusip $uushost"
#			echo "$uus"
#			krepp="$(grep -C0 -e $uusip -e $uushost $file)"
#			if [ ! -z  "${krepp}" ]; then
#				cases		
#				exit 1
#			else 
#			echo "$uus" >> $file
#			
#		fi
#		;;
		[nN] )
			exit 0
		;;
	esac
}

#P6hifunktsioon, kus pidu peale hakkab
if ipvalid "$ADDR"; then
	if [ ! -z "${crepe}" ]; then
		cases
		exit 0
	else
		echo -e "$HOSTN""\t\t\t" "$ADDR" >> "$file"
		echo "Kirje lisatud"
		exit 0
	fi
	service bind9 reload
else 
	echo "Sisestatud IP pole korrektne"
	exit 1
fi
